#!/bin/bash

set -e

function acomp {
    asciidoctor \
        -a doctype=article \
        -a linkcss \
        -a nofooter \
        -a source-highlighter=highlight.js \
        -a stem=latexmath \
        -a stylesheet=/style.css \
        -a toc \
        -a toclevels=5 \
        "$@"
}

rm ./public/* -r 2> /dev/null || :

cd src
while IFS= read -d '' -r file; do
   path="$(dirname "$file")"
   filename="$(basename "$file")"
   extless="${filename%.*}"
   pretty_path="~${path#.}"

   {
       echo "= $pretty_path"
       echo
       if [ "$path" != '.' ]; then echo 'link:..[up dir ↑]'; fi
       echo
       cat "$file"
       echo
   } > tmp.1

   while IFS= read -d '' -r dir; do
       reldir="$(basename "$dir")"
       serialized="${reldir// /%20}"
       echo 'link:'"$serialized"'[+++'"$reldir"'+++]' >> tmp.1
       echo >> tmp.1
   done < <(find "$path" -type d -maxdepth 1 -mindepth 1 -print0 | sort -nz)

   acomp tmp.1 -o "../public/$path/$extless.html"
   rm tmp.1
done < <(find . -type f -name "*.adoc" -print0)

while IFS= read -d '' -r file; do
    cp "$file" ../public/"$file"
done < <(find . -type f -not -name "*.adoc" -print0)
cd ..

cp assets/* public -r
