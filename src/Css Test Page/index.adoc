== Text Formatting

This is a paragraph.
Or rather, two sentences.

[.lead]
This is taken directly from the https://docs.asciidoctor.org/asciidoc/latest/syntax-quick-reference/[Asciidoctor quick reference].

Here is some *strong*, _stressed_, `monospace`, *_strongly stressed_*, and `*_monospace strongly stressed_*` text.

**S**ometimes you need to p__ut__ stress or ``stren``gth or monospace on random **__str__**ings.

Finally, here's #some# high##lighting##, [.underline]#some# under[.underline]##lining##, [.line-through]#some# strike[.line-through]##through##...

As well as some ^super^script and ~sub~script.

== Links

=== Anchor A

[[anchor-a]]This is an anchor.

[#anchor-b]#And this is another.#

=== Links

http://example.com/

http://example.com/[Example page]

example@example.com

mailto:example@example.com[More example mail]

=== Does Anchor A Work?

<<anchor-a>> should lead to Anchor A and <<anchor-b>> should lead to Anchor B.

== Sections

=== Up to

==== 4

===== levels

[discrete]
== Haha! I'm not in the TOC!

== Lists

=== Unordered

* I'm very unordered
** Look
*** At me!
** So little order!

// -

* How
** far
*** down
**** does
***** this
****** go?

=== Ordered
. Here's
. An
. Ordered
. List
.. With some
.. Substeps!

=== Checklist
* [ ] walk the cat
* [x] walk the fish
* [ ] put the pizza in bed
* [x] put the baby in the oven

=== Others

First term:: The description can be placed on the same line
as the term.
Second term::
Description of the second term.
The description can also start on its own line.

// -

[qanda]
What is the answer?::
This is the answer.

Are cameras allowed?::
Are backpacks allowed?::
No.

// -

Operating Systems::
  Linux:::
    . Fedora
      * Desktop
    . Ubuntu
      * Desktop
      * Server
  BSD:::
    . FreeBSD
    . NetBSD

== Images

[link=https://en.wikipedia.org/wiki/Test_card]
image::./test.png[Test card]

Here's an inline image:./test.png[Test card] image.

== Audio
audio::./amenbreak.wav[]

== Video
video::./vid.webm[]

== Literals
This is `+literal text+`.

 This is a literal line.

.Literal Block
....
This is a literal
✨block✨
....

.Source Code
[,python]
----
compose = lambda f, g: lambda x: f(g(x)) # <1>
----
<1> This function takes two functions and composes them.

== Admonitions

NOTE: This is an admonition

IMPORTANT: Children should not go in the oven

TIP: Glue is very yummy

CAUTION: The floor is wet

WARNING: The floor is still wet

== Tables

.Table Title
|===
|Column 1, Header Row |Column 2, Header Row
|Cell in column 1, row 1 |Cell in column 2, row 1
|Cell in column 1, row 2 |Cell in column 2, row 2
|===

[%header,cols=2*]
|===
|Name of Column 1 |Name of Column 2
|Cell in column 1, row 1 |Cell in column 2, row 1
|Cell in column 1, row 2 |Cell in column 2, row 2
|===

'''

== Biliography

=== Content

We need not to be let alone. We need to be really bothered once in a while. <<Bradbury>>

[bibliography]
=== References

* [[[Bradbury]]] Fahrenheit 451, Ray Bradbury

== Footnotes

Hey, this is a footnote!footnote:[Wow, you know how to click footnotes! Good for you!]

== Padding
image::./test.png[]
image::./test.png[]
image::./test.png[]
image::./test.png[]

== Other

Here's some stem:[\mathbb{LATEX}]~

[stem]
++++
\sqrt4=2
++++

=== Here are
=== some extra
=== levels
