set -e
timedatectl list-timezones
read -p 'Timezone: ' TIMEZONE

timedatectl set-timezone "$TIMEZONE"
timedatectl set-ntp 1

clear
fdisk -l
read -p 'Disk to install to: ' DISK

clear
read -p 'Prefix before partition numbers (usually empty or "p"): ' PARTITION_PREFIX

clear
echo 'Partition layout (UEFI):'
echo 'boot[1G,EFI] swap[~4G,SWAP] root[rest,LINUX]'
read -p 'Enter to continue...'

cfdisk "$DISK"
mkfs.ext4 "${DISK}${PARTITION_PREFIX}3"
mkswap "${DISK}${PARTITION_PREFIX}2"
mkfs.fat -F32 "${DISK}${PARTITION_PREFIX}1"

swapon "${DISK}${PARTITION_PREFIX}2"
mount "${DISK}${PARTITION_PREFIX}3" /mnt
mount --mkdir "${DISK}${PARTITION_PREFIX}1" /mnt/boot

clear
echo 'Set ParallelDownloads, enable multilib (optional), add an `ILoveCandy` line (optional)'
read -p 'Enter to continue...'
vim /etc/pacman.conf

mkdir -p /mnt/etc/
genfstab -U /mnt >> /mnt/etc/fstab
pacstrap -K /mnt base base-devel linux linux-firmware git less vim

clear
echo 'Run `arch-chroot /mnt` and proceed from there.'
