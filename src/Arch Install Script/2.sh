set -e
read -p 'Enter the timezone from before: ' TIMEZONE
ln -sf "/usr/share/zoneinfo/$TIMEZONE" /etc/localtime
hwclock --systohc

echo 'Uncomment your locale.'
read -p 'Enter to continue...'
vim /etc/locale.gen
clear
locale-gen
read -p 'Default locale: ' LOCALE
echo "LANG=$LOCALE" > /etc/locale.conf

clear
read -p 'Keyboard layout (likely `us`): ' KB_LAYOUT
echo "KEYMAP=$KB_LAYOUT" > /etc/vconsole.conf

clear
read -p 'Hostname: ' HOSTNAME
echo "$HOSTNAME" > /etc/hostname
echo '127.0.0.1 localhost' > /etc/hosts
echo '::1       localhost' >> /etc/hosts
echo "127.0.1.1 $HOSTNAME" >> /etc/hosts

clear
read -p 'Use custom MAC Address? (y/n) ' USE_MAC
if [ "$USE_MAC" = "y" ]; then
    clear
    ip link
    read -p 'Current MAC Address: ' CURRENT_MAC
    read -p 'New MAC Address: ' NEW_MAC
    echo "ACTION==\"add\", SUBSYSTEM==\"net\", ATTR{address}==\"$CURRENT_MAC\", RUN+=\"/usr/bin/ip link set dev \$name address $NEW_MAC\"" > /etc/udev/rules.d/81-mac-spoof.rules
fi

clear
echo 'ROOT'
passwd

clear
read -p 'Custom shells (list as pacman packages): ' SHELLS
pacman -S --noconfirm --needed $SHELLS

while true; do
    clear
    read -p 'Add a new user? (y/n) ' ANOTHER
    if [ "$ANOTHER" != 'y' ]; then
        break
    fi
    read -p 'Name: ' NAME
    read -p 'Sudo (Y for nopasswd)? (y/Y/n) ' SUDO
    cat /etc/shells
    read -p 'Shell: ' LOGIN_SHELL
    useradd -m "$NAME" -s "$LOGIN_SHELL"
    passwd "$NAME"
    if [ "$SUDO" = "y" ]; then
        echo "$NAME ALL=(ALL:ALL) ALL" >> /etc/sudoers
    elif [ "$SUDO" = "Y" ]; then
        echo "$NAME ALL=(ALL:ALL) NOPASSWD: ALL" >> /etc/sudoers
    fi
done

pacman -S --noconfirm --needed networkmanager grub efibootmgr
systemctl enable NetworkManager.service

while true; do
    read -p "Use [g]rub or [s]ystemd-boot: " BOOTLOADER
    if [ "$BOOTLOADER" == "g" ]; then
        clear
        read -p 'Bootloader ID: ' BL_ID
        grub-install --target=x86_64-efi --efi-directory /boot --bootloader-id="$BL_ID"
        grub-mkconfig -o /boot/grub/grub.cfg
    elif [ "$BOOTLOADER" == "s" ]; then
        bootctl install
        echo 'default arch' > /boot/loader/loader.conf
        echo 'timeout 3' >> /boot/loader/loader.conf
        vim /boot/loader/entries/arch.conf +'norm! ititle   Archlinux   /vmlinuz-linuxinitrd  /initramfs-linux.imgoptions root=UUID=V:!grep "/ " /etc/fstabdf=f	DkJx:wq'
    fi
done



clear
echo 'Now exit the chroot.'
echo 'Run `cp /etc/pacman.conf /mnt/etc/pacman.conf`'
echo 'Continue after logging into the installed system.'
